// hacker rank question: https://www.hackerrank.com/challenges/staircase/problem

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static void printStaircase(int n) {
        for(int i = 1;i <= n;i++){
            for(int k=i;k<n;k++)
                {
                    System.out.print(" ");
                }
            for(int j = 1; j <= i; j++){
                System.out.print("#");
            }
            System.out.println();
        }

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        printStaircase(n);

        scanner.close();
    }
}
