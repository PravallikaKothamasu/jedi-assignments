//hacker rank question: https://www.hackerrank.com/challenges/encryption/problem

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {
    static String encryption(String input) {
        
        List<String> listOfDividedWords = divideToWords(input);
        return getResultantString(listOfDividedWords);
    }  

    static List<String> divideToWords(String input){
        int length = input.trim().length();
        int rows = (int)(Math.floor(Math.sqrt(length)));
        int columns = (int)(Math.ceil(Math.sqrt(length)));
        if(!(rows * columns >= length)){
            rows+=1;
        }
        int index;
        List<String> words = new ArrayList<>();
        for(index = 0; length-index>=columns ; index+=columns){
            words.add(input.substring(index,index+columns));
        }
        if(!(index==length)){
            words.add(input.substring(index));
        }

        return words;
    }

    static String getResultantString(List<String> words){
        String result="";
        for(int i=0; i<=words.size();i++){
            for(int j=0;j<words.size();j++){
                if(i<words.get(j).length()){
                    result+=words.get(j).charAt(i);
                }
            }
           result+=" ";
        }
        return result;
    }                             

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scanner.nextLine();

        String result = encryption(s);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
