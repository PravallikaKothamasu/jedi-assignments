import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
public class Solution {

    // Complete the matrixRotation function below.
    static void matrixRotation(List<List<Integer>> matrix, int r) {
       if(checkIfAllElementsInMatrixAreEqual(matrix)){
           printMatrix(matrix);
       }else{
            //for(int rotations=0;rotations<1;rotations++){
                printMatrix(rotateMatrix(matrix));
            //}
       }

    }

    static List<List<Integer>> rotateMatrix(List<List<Integer>> matrix){
        System.out.println("inside rotateMatrix");
        int m=matrix.size();
        int n=matrix.get(0).size();
        int borders=m/2;
        List<Integer> innerList = new ArrayList<Integer>();
        List<List<Integer>> resultantMatrix = new ArrayList<>();
         for(int i=0;i<m;i++){
                resultantMatrix.add(i,innerList);
        }
        System.out.println(resultantMatrix);
        System.out.println(m);
        System.out.println(n);
        for(int i=0,k=n,b=n;i<borders;i++,k--,b=b-2){
            for(int j=i;j<b;j++){
                if(j<n){
                    if(j==n-i-1){
                        //resultantMatrix.get(i).get(j)=matrix.get(i+1).get(j);
                        resultantMatrix.get(i).add(j,matrix.get(i+1).get(j));
                        System.out.println("this:"+matrix.get(i+1).get(j));
                    }else{
                         resultantMatrix.get(i).add(j,matrix.get(i).get(j+1));
                        System.out.println("that"+matrix.get(i).get(j+1));
                        System.out.println("inserted at:"+resultantMatrix.get(i).get(j));
                    }
                }
               // System.out.println(i);
                 //System.out.println(j);
                    resultantMatrix.get(j+1).add(i,matrix.get(j).get(i));
                    if(j<k-1){
                        resultantMatrix.get(k).add(j+1,matrix.get(k).get(j));
                        resultantMatrix.get(j+1).add(k-1,matrix.get(j+2).get(k-1));
                    }
                }
                //System.out.println(matrix.get(i+1).get(j));
            }
            System.out.println("resultant matrix:"+resultantMatrix);
        return resultantMatrix;
    }

    static void printMatrix(List<List<Integer>> matrix){
        for(int i=0;i<matrix.size();i++){
            for(int j=0;j<matrix.get(i).size();j++){
            System.out.print(matrix.get(i).get(j)+" ");
            }
             System.out.println();
        }
    }

    static boolean checkIfAllElementsInMatrixAreEqual(List<List<Integer>> matrix){
        boolean result=true;
        for(int i=0;i<matrix.size()-1;i++){
            if(!(matrix.get(i).equals(matrix.get(i+1))))
            {
                result=false;
                break;
            }
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        String[] mnr = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

        int m = Integer.parseInt(mnr[0]);

        int n = Integer.parseInt(mnr[1]);

        int r = Integer.parseInt(mnr[2]);

        List<List<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            String[] matrixRowTempItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

            List<Integer> matrixRowItems = new ArrayList<>();

            for (int j = 0; j < n; j++) {
                int matrixItem = Integer.parseInt(matrixRowTempItems[j]);
                matrixRowItems.add(matrixItem);
            }

            matrix.add(matrixRowItems);
        }

        matrixRotation(matrix, r);

        bufferedReader.close();
    }
}
