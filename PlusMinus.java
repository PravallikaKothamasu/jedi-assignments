// hacker rank question: https://www.hackerrank.com/challenges/plus-minus/problem

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static void plusMinus(int[] arr) {
        int countOfPositives = 0;
        int countOfNegatives = 0;
        int countOfZeroes = 0;
        int lengthOfArray = arr.length;
        for(int i = 0; i < lengthOfArray; i++){
            if(arr[i] == 0)
                {
                    countOfZeroes++;
                }else if(arr[i] < 0){
                    countOfNegatives++;
                }else{
                    countOfPositives++;
                }
        }
        System.out.println((float)countOfPositives/lengthOfArray);
        System.out.println((float)countOfNegatives/lengthOfArray);
        System.out.println((float)countOfZeroes/lengthOfArray);
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
